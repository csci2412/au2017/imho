import SystemController from '../modules/SystemController.js';

(() => {
    const controller = new SystemController();

    document.querySelector('#systems').addEventListener('click', e => {
        const el = e.target;

        if (el.classList.contains('delete-btn')) {
            controller.deleteSystem(el.parentElement.dataset.id);
        }

        if (el.classList.contains('edit-btn')) {
            controller.editSystem(el.parentElement.dataset.id);
        }
    });

    document.querySelector('#add-btn').addEventListener('click',
        () => controller.saveSystem());
})()
