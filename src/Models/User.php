<?php
namespace Imho\Models;

final class User
{
    public $id;
    public $username;
    public $isAdmin;
    public $hash;

    public function __construct(array $props = [])
    {
        foreach ($props as $name => $val) {
            switch ($name) {
                case 'id':
                    $this->id = $val;
                    break;
                case 'username':
                    $this->username = $val;
                    break;
                case 'is_admin':
                    $this->isAdmin = $val;
                    break;
                case 'password':
                    $this->hash = $val;
                    break;
            }
        }
    }

    public function validateLogin($password)
    {
        return password_verify($password, $this->hash);
    }
}
