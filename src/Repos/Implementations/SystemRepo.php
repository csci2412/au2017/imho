<?php
namespace Imho\Repos\Implementations;

use \Imho\Repos\Interfaces\ISystemRepo as ISystemRepo;

final class SystemRepo implements ISystemRepo
{

    /**
    * @Inject("logger")
    */
    private $logger;

    /**
    * @Inject("dbConnection")
    */
    private $conn;

    public function getSystems() : array
    {
        return $this->conn->fetchAll('SELECT * FROM system ORDER BY title');
    }

    public function deleteSystem(int $id) : bool
    {
        try {
            $sql = 'DELETE FROM system WHERE id = ?';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(1, $id);

            $success = $stmt->execute();

            if (!$success) {
                throw new \Exception($stmt->error);
            }

            return true;
        } catch (\Throwable $e) {
            $this->logger->info($e->getMessage());
            throw $e;
        }
    }

    public function editSystem(int $id, string $name) : bool
    {
        try {
            $sql = 'UPDATE system SET title = ? where id = ?';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(1, $name);
            $stmt->bindValue(2, $id);

            $success = $stmt->execute();

            if (!$success) {
                throw new \Exception($stmt->error);
            }

            return true;
        } catch (\Throwable $e) {
            $this->logger->info($e->getMessage());
            throw $e;
        }
    }

    public function addSystem(string $name) : int
    {
        try {
            $sql = 'INSERT INTO system (title) VALUES(?)';
            $stmt = $this->conn->prepare($sql);
            $stmt->bindValue(1, $name);

            $success = $stmt->execute();

            if (!$success) {
                throw new \Exception($stmt->error);
            }

            return $this->conn->lastInsertId();
        } catch (\Throwable $e) {
            $this->logger->info($e->getMessage());
            throw $e;
        }
    }
}
