<?php
namespace Imho\Services\Interfaces;

use \Imho\Models\User;

interface IAccountService
{
    public function createNewUser(string $username, string $pass, string $confirm) : bool;
    public function getUser(string $username) : User;
}
