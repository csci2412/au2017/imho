<?php
namespace Imho\Services\Interfaces;

use \Imho\Models\Game;

interface IGameService {
  public function getGames() : array;
  public function getGame(int $id) : Game;
  public function deleteGame(int $id) : bool;
  public function addGame(string $title, int $sysId, int $year, bool $completed) : int;
  public function editGame(int $id, string $title, int $sysId, int $year, bool $completed) : bool;
}
