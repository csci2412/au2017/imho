drop database if exists gamedb;
create database if not exists gamedb;
use gamedb;

create table `user`(
	`id` int unsigned auto_increment,
    `username` nvarchar(255),
    `password` char(60),
    `date_created` datetime default current_timestamp,
    `last_updated` timestamp,
	`is_admin` bool default false,
    primary key(`id`),
    unique (`username`)
);

create table `system`(
	`id` int unsigned auto_increment,
    `title` nvarchar(255),
    `date_created` datetime default current_timestamp,
    `last_updated` timestamp,
    primary key(`id`)
);

create table `game`(
	`id` int unsigned auto_increment,
    `title` nvarchar(255) not null,
    `system_id` int unsigned not null,
    `release_year` year not null,
    `completed` bool default false,
    primary key(`id`),
    foreign key(`system_id`) references `system`(`id`)
);

create table `category`(
	`id` int unsigned auto_increment,
    `label` varchar(255),
    `date_created` datetime default current_timestamp,
    `last_updated` timestamp,
    primary key(`id`)
);

create table `game_category`(
	`game_id` int unsigned not null,
    `category_id` int unsigned not null,
    primary key(`game_id`, `category_id`),
    foreign key(`game_id`) references `game`(`id`),
    foreign key(`category_id`) references `category`(`id`)
);

create table `blog`(
	`id` int unsigned auto_increment,
    `user_id` int unsigned not null,
    `game_id` int unsigned,
    `title` nvarchar(255),
    `body` longtext,
    `date_created` datetime default current_timestamp,
    `last_updated` timestamp,
    primary key(`id`),
    foreign key(`game_id`) references `game`(`id`),
    foreign key(`user_id`) references `user`(`id`)
);

create table `comment`(
	`id` int unsigned auto_increment,
    `user_id` int unsigned not null,
    `blog_id` int unsigned not null,
    `date_created` datetime default current_timestamp,
    `last_updated` timestamp,
    primary key(`id`),
    foreign key(`user_id`) references `user`(`id`),
    foreign key(`blog_id`) references `blog`(`id`)
);

insert into system(title) values('SNES'), ('PC'), ('PS2'), ('PS3'), ('Dreamcast'), ('Gamecube');
